# OS upgrade

Ansible role to upgrade Operating System.

## OS
* Debian only right now (RedHat planned)

## Requirements

* Ansible >= 4.

## Role Variables

Variable name       | Description
---                 | ---
os_upgrade_packages | list of packages to install
reboot_authorized   | Reboot server if kernel upgraded (false or true)

## Dependencies

No.

## Playbook Example

An example of playbook

```
- name: Upgrade Operating system
  hosts: all
  become: yes
  roles:
    - os_upgrade
```

## License

* GPL v3

## Author Information

* [Stéphane Paillet](mailto:spaillet@ethicsys.fr)

