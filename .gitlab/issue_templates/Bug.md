# Rapport de bug

## Informations
- Nom du playbook ou rôle :
- Version :
- Date du bug :

## Description
Faites une description du bug rencontré.

## Étapes pour reproduire le bug
Décrivez précisément les étapes que vous avez suivies pour voir apparaître ce bogue.
