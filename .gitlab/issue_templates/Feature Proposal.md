# Proposition d'évolution

## Informations
- Playbook ou rôle :
- Type de fonctionnalité :

## Description
(Inclure le problème, les cas d'utilisation, les avantages et / ou les objectifs)

## Proposition
Décrivez votre proposition.

## Liens / références
Listez les liens et références éventuelles.

## Documentation
(Écrivez le début de la documentation de cette fonctionnalité ici, incluez :

* Pourquoi quelqu'un devrait-il l'utiliser ?
* Quel est le problème sous-jacent ?
* Quelle est la solution ?
* Comment quelqu'un utilise-t-il cela ?

Pendant la mise en œuvre, cela peut être copié et utilisé comme point de départ pour la documentation).
